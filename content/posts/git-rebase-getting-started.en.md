---
title: "Git Rebase: Getting Started"
date: 2021-02-26T16:09:00Z
draft: false
author: "fossdd"
categories: ["git", "how-to"]
tags: ["git", "how-to"]
---

Maybe you had the problem: You have a fork of a repo. But the fork isn't up to date with the upstream.. On the GitLab/GitHub/Gitea UI you don't see a feature for updating your fork. Sometimes you can create a PR/MR on your fork with the upstream as source. But a PR for every new commit is bad..

So, Git has a feature for that. Maybe not explicitly for the "fork" model. But does exactly what you want.

## What are remotes?

First, we'll short cover what are remotes.

Remotes are in a local git repository. If you type `git push`, it sends the commits not existing on the remote, to the remote. So, a remote is a git server like GitHub and Codeberg.

To see active remotes in your repo type `git remote -v`. (`-v` is prints more output. Without it you see only the names)

Maybe, if you cloned from a remote server the repo, it looks like:

```shell
$ git remote -v
origin	git@gitlab.com:fossdd/fdroiddata.git (fetch)
origin	git@gitlab.com:fossdd/fdroiddata.git (push)
```

For the basic knowledge, you can ignore the doubled remotes. So, this remote registered, is called `origin`. And the url is `git@gitlab.com:fossdd/fdroiddata.git`. Also if this is a forked repo, you only see the forked repo url.

So first we add the upstream as a remote called `upstream`.

```shell
$ git remote add upstream git@gitlab.com:fdroid/fdroiddata
```

And the `git remote -v` looks like:
```shell
$ git remote -v
origin	git@gitlab.com:fossdd/fdroiddata.git (fetch)
origin	git@gitlab.com:fossdd/fdroiddata.git (push)
upstream	git@gitlab.com:fdroid/fdroiddata.git (fetch)
upstream	git@gitlab.com:fdroid/fdroiddata.git (push)
```

So, our two remotes are registered. The first one is the fork repo, and second is the upstream where the fork comes from.

# Preparing things

First run `git fetch upstream`. With that we download all branches and refs available of the remote specified, in our case `upstream`.

```shell
$ git fetch upstream
remote: Enumerating objects: 217, done.
remote: Counting objects: 100% (182/182), done.
remote: Compressing objects: 100% (72/72), done.
remote: Total 133 (delta 98), reused 93 (delta 61), pack-reused 0
Receiving objects: 100% (133/133), 18.45 KiB | 419.00 KiB/s, done.
Resolving deltas: 100% (98/98), completed with 28 local objects.
From gitlab.com:fdroid/fdroiddata
   322c3b4524..da8a2a6fbb  master     -> upstream/master
```

The next step is to checkout our branch we want to rebase.

For example if you have an MR/PR on branch `x` checkout the branch `x` and run the next commands. If you still want to update the master or the main branch check this out.

# Now: Rebase!

Not let's rebase with `git rebase upstream/master`. You can of course also rebase a other branch of your repo instead of the remote. Do that with `git remote my-branch-with-other-commits`. Or a other branch on the remote like `git rebase remote/branch-x`.

```shell
$ git rebase upstream/master
Successfully rebased and updated refs/head/master
```

Great! Now our HEAD is up to date with `upstream/master`

# Push it to your origin

Let's push it to our `origin` remote with the fork located.

```shell
$ git push origin master
Enumerating objects: 217, done.
Counting objects: 100% (182/182), done.
Delta compression using up to 6 threads
Compressing objects: 100% (35/35), done.
Writing objects: 100% (133/133), 18.45 KiB | 18.45 MiB/s, done.
Total 133 (delta 98), reused 133 (delta 98), pack-reused 0
remote: Resolving deltas: 100% (98/98), completed with 28 local objects.
To gitlab.com:fossdd/fdroiddata.git
   322c3b4524..da8a2a6fbb  master -> master
```

And then look in your web UI of the remote and be happy!
