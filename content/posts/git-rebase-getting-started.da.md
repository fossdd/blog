---
title: " Git Rebase: Kom godt i gang "
date: 2021-02-26T16:09:00Z
draft: false
author: "fossdd"
categories: ["git", "how-to"]
tags: ["git", "how-to"]
---

 Måske har du haft problemet: Du har en fork af en repo, men gaflen er ikke opdateret med upstream... På GitLab/GitHub/Gitea UI kan du ikke se en funktion til at opdatere din fork. Nogle gange kan du oprette en ny PR/MR på din fork med opstrømmen som kilde, men en PR for hvert nyt commit er dårligt… 

 Så Git har en funktion til det. Måske ikke udtrykkeligt for "fork"-modellen. Men den gør præcis det, du ønsker. 

## Hvad er fjernbetjening? 

 Først vil vi kort beskrive, hvad fjernbetjeninger er. 

 Remotes er i et lokalt Git-repository. Hvis du skriver `git push`, sender det de kommits, der ikke findes på fjernoplaget, til det. Så en remote er en Git-server, som GitHub og Codeberg. 

 Du kan se aktive fjernindstillinger i dit arkiv ved at skrive `git remote -v` for at se aktive fjernindstillinger i dit arkiv. (`-v` giver mere output. Uden det ser du kun navnene.) 

 Måske, hvis du klonede repo'en fra en ekstern server, ser det ud som: 

```shell
$ git remote -v
origin	git@gitlab.com:fossdd/fdroiddata.git (fetch)
origin	git@gitlab.com:fossdd/fdroiddata.git (push)
```

 For den grundlæggende viden kan du ignorere de dobbelte fjernbetjeninger. Så denne fjernbetjening, der er registreret, kaldes `origin`. Og URL'en er `git@gitlab.com:fossdd/fdroiddata.git`. Hvis dette også er en forked repo, ser du kun URL'en forked repo. 

 Så først tilføjer vi upstream som en fjernbetjening kaldet `upstream`. 

```shell
$ git remote add upstream git@gitlab.com:fdroid/fdroiddata
```

 Og `git remote -v` ser således ud: 
```shell
$ git remote -v
origin	git@gitlab.com:fossdd/fdroiddata.git (fetch)
origin	git@gitlab.com:fossdd/fdroiddata.git (push)
upstream	git@gitlab.com:fdroid/fdroiddata.git (fetch)
upstream	git@gitlab.com:fdroid/fdroiddata.git (push)
```

 Og `git remote -v` ser således ud: 

#  Klargøring af ting 

 Kør først `git fetch upstream`. Dermed henter vi alle tilgængelige branches og refs fra den fjernbetjening, der er angivet, i vores tilfælde `upstream`. 

```shell
$ git fetch upstream
remote: Enumerating objects: 217, done.
remote: Counting objects: 100% (182/182), done.
remote: Compressing objects: 100% (72/72), done.
remote: Total 133 (delta 98), reused 93 (delta 61), pack-reused 0
Receiving objects: 100% (133/133), 18.45 KiB | 419.00 KiB/s, done.
Resolving deltas: 100% (98/98), completed with 28 local objects.
From gitlab.com:fdroid/fdroiddata
   322c3b4524..da8a2a6fbb  master     -> upstream/master
```

 Det næste skridt er at tjekke vores gren, som vi ønsker at lave en ny base. 

 Hvis du f.eks. har en MR/PR på gren `x`, så tjek gren `x` ud og kør de næste kommandoer. Hvis du stadig ønsker at opdatere master- eller hovedgrenen, skal du tjekke dette. 

#  Nu: Rebase! 

 Ikke lad os lave en ny base med `git rebase upstream/master`. Du kan selvfølgelig også rebasere en anden gren af dit repo i stedet for den eksterne. Det gør du med `git remote my-branch-with-other-commits`. Eller en anden branch på fjerneren som `git rebase remote/branch-x`. 

```shell
$ git rebase upstream/master
Successfully rebased and updated refs/head/master
```

 Fantastisk! Nu er vores HEAD opdateret med `upstream/master` 

# Skub den til din oprindelse 

 Skub den til din oprindelse 

```shell
$ git push origin master
Enumerating objects: 217, done.
Counting objects: 100% (182/182), done.
Delta compression using up to 6 threads
Compressing objects: 100% (35/35), done.
Writing objects: 100% (133/133), 18.45 KiB | 18.45 MiB/s, done.
Total 133 (delta 98), reused 133 (delta 98), pack-reused 0
remote: Resolving deltas: 100% (98/98), completed with 28 local objects.
To gitlab.com:fossdd/fdroiddata.git
   322c3b4524..da8a2a6fbb  master -> master
```

 Og så kan du kigge på fjernbetjeningen på din webbrugergrænseflade og være glad! 
