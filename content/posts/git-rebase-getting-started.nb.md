---
title: " Git-rebasering: En veiledning "
date: 2021-02-26T16:09:00Z
draft: false
author: "fossdd"
categories: ["git", "how-to"]
tags: ["git", "how-to"]
---

 Git-rebasering: En veiledning 

 Git har en funksjon for dette. Kanskje ikke akkurat i modellen for «forgrening». Det gjør dog akkurat det du ønsker. 

##  Hva er fjernkodelager? 

 Hva er fjernkodelager? 

 Fjernkodelagre er i et lokalt Git-kodelager. Hvis du skriver `git push` sender det innsendelsene som ikke finnes på fjernkodelageret dit. Så, et fjernkodelager er en Git-tjener som GitHub og Codeberg. 

 For å se aktive fjernkodelagre i ditt kodelager kan du skrive `git remote -v`. (`-v` skriver ut mer. Uten det ser du kun navnene.) 

 Kanskje, hvis du klonet fra en ekstern server repo, ser det ut som: 

```shell
$ git remote -v
origin	git@gitlab.com:fossdd/fdroiddata.git (fetch)
origin	git@gitlab.com:fossdd/fdroiddata.git (push)
```

 For enkelhets skyld kan du ignorere de doblede fjernkodelagerne. Så, dette fjernkodelageret er registrert, det heter `origin`. Og nettadressen er `git@gitlab.com:fossdd/fdroiddata.git`. Hvis dette er et forgrenet kodelager ser du kun den forgrenede kodelagernettadressen. 

 Først må oppstrøm legges til som et fjernkodelager kalt `upstream`. 

```shell
$ git remote add upstream git@gitlab.com:fdroid/fdroiddata
```

 Og `git remote -v` ser da slik ut: 
```shell
$ git remote -v
origin	git@gitlab.com:fossdd/fdroiddata.git (fetch)
origin	git@gitlab.com:fossdd/fdroiddata.git (push)
upstream	git@gitlab.com:fdroid/fdroiddata.git (fetch)
upstream	git@gitlab.com:fdroid/fdroiddata.git (push)
```

 Så, våre to fjernkodelager er registrert. Det første er det forgrenede kodelageret, og det andre er oppstrømmen forgreningen kom fra. 

# Forberedelser

 Først kjører du `git fetch upstream`. Da har du lastet ned alle forgreninger og referanser tilgjengelig for fjernkodelageret angitt, i vårt tilfelle `upstream`. 

```shell
$ git fetch upstream
remote: Enumerating objects: 217, done.
remote: Counting objects: 100% (182/182), done.
remote: Compressing objects: 100% (72/72), done.
remote: Total 133 (delta 98), reused 93 (delta 61), pack-reused 0
Receiving objects: 100% (133/133), 18.45 KiB | 419.00 KiB/s, done.
Resolving deltas: 100% (98/98), completed with 28 local objects.
From gitlab.com:fdroid/fdroiddata
   322c3b4524..da8a2a6fbb  master     -> upstream/master
```

 Neste steg er å utføre utsjekk av forgreningen man ønsker å rebasere. 

 For eksempel hvis du har et MR/PR på gren `x` checkout grenen `x` og kjøre de neste kommandoene. Hvis du fortsatt ønsker å oppdatere master eller main branch sjekke dette ut. 

#  Nå: Rebasering! 

 Vi rebaserer med `git rebase upstream/master`. Du kan også selvfølgelig rebasere en annen forgrening av ditt kodelager istedenfor fjernkodelageret. Gjør dette med `git remote min-forgrening-med-andre-innsendelser`. Eller en annen forgrening på fjernkodelageret som `git rebase remote/branch-x`. 

```shell
$ git rebase upstream/master
Successfully rebased and updated refs/head/master
```

 Storartet. Nå er vårt HEAD oppdatert med `upstream/master` 

# Dytt det til ditt opphav 

 La oss presse den til vår "opprinnelse" remote med gaffelen ligger. 

```shell
$ git push origin master
Enumerating objects: 217, done.
Counting objects: 100% (182/182), done.
Delta compression using up to 6 threads
Compressing objects: 100% (35/35), done.
Writing objects: 100% (133/133), 18.45 KiB | 18.45 MiB/s, done.
Total 133 (delta 98), reused 133 (delta 98), pack-reused 0
remote: Resolving deltas: 100% (98/98), completed with 28 local objects.
To gitlab.com:fossdd/fdroiddata.git
   322c3b4524..da8a2a6fbb  master -> master
```

 Deretter kan du ta en titt i vev-grensesnittet for fjernkodelageret og være glad! 
