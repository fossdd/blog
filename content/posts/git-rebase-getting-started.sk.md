---
title: " Git Rebase: Začíname "
date: 2021-02-26T16:09:00Z
draft: false
author: "fossdd"
categories: ["git", "how-to"]
tags: ["git", "how-to"]
---

 Možno ste mali tento problém: máte fork repo, ale tento fork nie je aktualizovaný s upstreamom... V používateľskom rozhraní GitLab/GitHub/Gitea nevidíte funkciu na aktualizáciu vášho forku. Niekedy môžete vytvoriť nový PR/MR na svojej fork s upstreamom ako zdrojom, ale PR pre každú novú revíziu je zlé… 

 Git má na to funkciu. Možno nie výslovne pre model "fork". Ale robí presne to, čo chcete. 

##  Čo sú to diaľkové ovládače? 

 Najprv si stručne vysvetlíme, čo sú to diaľkové ovládače. 

 Vzdialené súbory sú v miestnom úložisku Git. Ak zadáte `git push`, odošlú sa doň revízie, ktoré neexistujú na vzdialenom úložisku. Vzdialený server je teda server Git, ako napríklad GitHub a Codeberg. 

 Ak chcete zobraziť aktívne vzdialené súbory v úložisku, zadajte `git remote -v`. (`-v` poskytuje viac výstupu. Bez neho vidíte len názvy.) 

 Ak chcete zobraziť aktívne vzdialené súbory v úložisku, zadajte `git remote -v`. (`-v` poskytuje viac výstupu. Bez neho vidíte len názvy.) 

```shell
$ git remote -v
origin	git@gitlab.com:fossdd/fdroiddata.git (fetch)
origin	git@gitlab.com:fossdd/fdroiddata.git (push)
```

 Pre základné znalosti môžete ignorovať zdvojené diaľkové ovládanie. Takže tento registrovaný vzdialený ovládač sa nazýva `pôvodný`. A adresa URL je `git@gitlab.com:fossdd/fdroiddata.git`. Taktiež ak ide o forknuté repo, vidíte iba URL adresu forknutého repa. 

 Najprv teda pridáme upstream ako vzdialený server s názvom `upstream`. 

```shell
$ git remote add upstream git@gitlab.com:fdroid/fdroiddata
```

 A `git remote -v` vyzerá takto: 
```shell
$ git remote -v
origin	git@gitlab.com:fossdd/fdroiddata.git (fetch)
origin	git@gitlab.com:fossdd/fdroiddata.git (push)
upstream	git@gitlab.com:fdroid/fdroiddata.git (fetch)
upstream	git@gitlab.com:fdroid/fdroiddata.git (push)
```

 Takže naše dva diaľkové ovládače sú zaregistrované. Prvý je repozitár fork a druhý je upstream, odkiaľ fork pochádza. 

# Príprava vecí 

 Príprava vecí 

```shell
$ git fetch upstream
remote: Enumerating objects: 217, done.
remote: Counting objects: 100% (182/182), done.
remote: Compressing objects: 100% (72/72), done.
remote: Total 133 (delta 98), reused 93 (delta 61), pack-reused 0
Receiving objects: 100% (133/133), 18.45 KiB | 419.00 KiB/s, done.
Resolving deltas: 100% (98/98), completed with 28 local objects.
From gitlab.com:fdroid/fdroiddata
   322c3b4524..da8a2a6fbb  master     -> upstream/master
```

 Ďalším krokom je kontrola našej vetvy, ktorú chceme rebasovať. 

 Napríklad ak máte MR/PR na vetve `x`, zaregistrujte vetvu `x` a spustite nasledujúce príkazy. Ak chcete ešte aktualizovať hlavnú alebo hlavnú vetvu, skontrolujte toto. 

#  Teraz: Rebase! 

 Nie prebazujeme pomocou `git rebase upstream/master`. Samozrejme, namiesto vzdialenej vetvy môžete rebasovať aj inú vetvu vášho repozitára. Urobte to príkazom `git remote my-branch-with-other-commits`. Alebo inú vetvu na vzdialenom reťazci ako `git rebase remote/branch-x`. 
 
```shell
$ git rebase upstream/master
Successfully rebased and updated refs/head/master
```

 Skvelé! Teraz je náš HEAD aktualizovaný pomocou `upstream/master` 

# Presuňte ho na svoj pôvod 

 Odošleme ho do nášho vzdialeného servera `origin` s umiestnenou vidlicou. 

```shell
$ git push origin master
Enumerating objects: 217, done.
Counting objects: 100% (182/182), done.
Delta compression using up to 6 threads
Compressing objects: 100% (35/35), done.
Writing objects: 100% (133/133), 18.45 KiB | 18.45 MiB/s, done.
Total 133 (delta 98), reused 133 (delta 98), pack-reused 0
remote: Resolving deltas: 100% (98/98), completed with 28 local objects.
To gitlab.com:fossdd/fdroiddata.git
   322c3b4524..da8a2a6fbb  master -> master
```

 A potom sa pozrite do webového používateľského rozhrania diaľkového ovládača a buďte šťastní! 
