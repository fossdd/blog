---
title: "Git Rebase: Lehen Urratsak"
date: 2021-02-26T16:09:00Z
draft: false
author: "fossdd"
categories: ["git", "how-to"]
tags: ["git", "how-to"]
---

Agian izan duzu arazoa: bat Duzu bidegurutze bat repo, baina bidegurutze ez eguneratuta upstream... On GitLab/GitHub/Gitea UI, ez duzu ikusten ezaugarri bat eguneratzeko zure sardexka. Batzuetan berri bat sor dezakezu PR/MR zure bidegurutze batera upstream-iturri gisa, baina PR bakoitzean berri konpromisoa da txarra…

Beraz, Git bat du ezaugarri hori. Agian ez da esplizituki egiteko "bidegurutze" eredua. Baina ez du zehazki zer nahi duzun. 

## Zer dira remotes? 

Lehen, egingo dugu labur estaltzeko zer dira remotes. 

Remotes dira tokiko Git biltegia. Baduzu, idatzi `git push`, bidaltzen da konpromisoa ez dauden urruneko da. Beraz, urruneko bat da Git zerbitzari bezala, GitHub eta Codeberg. 

Ikusi aktiboa remotes, zure biltegia, mota `git urruneko -v`. (`-v` bekak gehiago irteera. Hura gabe ikusiko duzu izenak bakarrik.) 

Agian, bada, klonatu urruneko zerbitzari bat du repo, itxura: 

```shell
$ git remote -v
origin	git@gitlab.com:fossdd/fdroiddata.git (fetch)
origin	git@gitlab.com:fossdd/fdroiddata.git (push)
```

Oinarrizko ezagutza, ikusi ahal izango duzu bikoiztu remotes. Beraz, hau urruneko emanda, deitzen da `jatorria`. Eta URL da `git@gitlab.com:fossdd/fdroiddata.git`. Era berean, bada, hau da, bat sardetutako repo, zuk bakarrik ikusi sardetutako repo URL. 

Beraz, lehen gehitu dugu upstream gisa, urruneko `izeneko ibaian gora`. 

```shell
$ git remote add upstream git@gitlab.com:fdroid/fdroiddata
```

Eta `git urruneko -v` itxura: 
```shell
$ git remote -v
origin	git@gitlab.com:fossdd/fdroiddata.git (fetch)
origin	git@gitlab.com:fossdd/fdroiddata.git (push)
upstream	git@gitlab.com:fdroid/fdroiddata.git (fetch)
upstream	git@gitlab.com:fdroid/fdroiddata.git (push)
```

Beraz, gure bi remotes erregistratu dira. Lehena da bidegurutze repo, eta bigarren da, ibaian gora, non bidegurutze batetik dator. 

# Gauzak prestatzen 

 Lehen exekutatu `git eskuratu upstream`. Hori dugu, download adar guztietan eta refs eskuragarri urruneko zehaztu, gure kasuan, `upstream`. 

```shell
$ git fetch upstream
remote: Enumerating objects: 217, done.
remote: Counting objects: 100% (182/182), done.
remote: Compressing objects: 100% (72/72), done.
remote: Total 133 (delta 98), reused 93 (delta 61), pack-reused 0
Receiving objects: 100% (133/133), 18.45 KiB | 419.00 KiB/s, done.
Resolving deltas: 100% (98/98), completed with 28 local objects.
From gitlab.com:fdroid/fdroiddata
   322c3b4524..da8a2a6fbb  master     -> upstream/master
```

 Hurrengo urratsa da checkout gure adar nahi dugu rebase. 

 Adibidez bat behar duzu bada MR/PR adar `x` eskaera egin adar `x` eta exekutatu hurrengo komandoak. Bada oraindik eguneratu nahi master edo adar nagusia begiratu hau. 

#  Orain: Rebase! 

 Ez dezagun rebase batera `git rebase upstream/master`. Ahal izango duzu, jakina, halaber, rebase bat beste adar zure repo ordez, urruneko. Egin hori `git urruneko nire-adarraren-batera-beste-konpromisoa`. Edo beste adar urruneko bezala `git rebase urruneko/sucursal-x`. 

```shell
$ git rebase upstream/master
Successfully rebased and updated refs/head/master
```

 Handia! Orain, gure BURUA eguneratuta `upstream/master` 

#  Push zure jatorria 

 Dezagun bultza gure `jatorria` urruneko batera bidegurutze dago. 

```shell
$ git push origin master
Enumerating objects: 217, done.
Counting objects: 100% (182/182), done.
Delta compression using up to 6 threads
Compressing objects: 100% (35/35), done.
Writing objects: 100% (133/133), 18.45 KiB | 18.45 MiB/s, done.
Total 133 (delta 98), reused 133 (delta 98), pack-reused 0
remote: Resolving deltas: 100% (98/98), completed with 28 local objects.
To gitlab.com:fossdd/fdroiddata.git
   322c3b4524..da8a2a6fbb  master -> master
```

 Eta, ondoren, begiratu zure web UI urruneko eta zoriontsu izan! 
