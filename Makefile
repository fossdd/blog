all: clone-public build

clone-public:
	rm -fr public
	git clone https://codeberg.org/fossdd/pages.git public

build:
	hugo --minify --enableGitInfo

push:
	cd public;git commit -a -m "deploy";git push
